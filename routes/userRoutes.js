
//object that handles client requests and send back responses
//property of express module
	//Router();

const express = require("express");
const router = express.Router();


//in order to use controllers module properties, import controllers module
const userController = require("./../controllers/userControllers");

//Retrieving array of documents from database using "GET" method and find() model method
router.get("/", (req, res) => {
	//check the request if there's data to be used
	//console.log(`This link is working`);


	//invoke the function from controllers module
	userController.getAllUsers().then( result => {
		res.send(result);
	})
});


//Add a user in the database using "POST" http method and save() method
router.post("/add-user", (req, res) => {


	//check the request if there's data to be used
	console.log(`add user is working ${req.body}`);
	
	//invoke the function from controllers module
	userController.register(req.body).then((result) => res.send(result));
})

router.get("/get-user", (req, res) => {

	//console.log(`get user is working`);
	
	//Data is captured
	//console.log(req.body);

	userController.getUser(req.body).then((result) => res.send(result));
});

router.put("/update-task", (req, res) => {
	//console.log(req.body);
	userController.updateTask(req.body).then((result) => res.send(result));
})

//in order for the routes to be use in other modules, we need to export it first
module.exports = router;

