// Import express module

const express = require("express");

// Import mongoose model

const mongoose = require("mongoose");

// Import express model

const app = express();

//Local port 

const PORT = process.env.PORT || 4000;

const userRoutes = require("./routes/userRoutes");


// Connecting MongoDB 

mongoose.connect("mongodb+srv://admin:admin@cluster0.qtkm7.mongodb.net/s31?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Mongodb notifications

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));

//Middleware

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//ROUTE FOR ACTIVITY
app.use("/api/users", userRoutes);
//Listen

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));

