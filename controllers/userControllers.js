
//to query or manipulate database, import the model module to use the model methods
const User = require('./../models/User');
	//find()  ---> array of documents []
	//findOne()
	//findOneAnd..
	//findByIdAnd..

//userController is a module for business logic/ API tasks to be done
module.exports.getAllUsers = () => {
	//console.log(`This is working`);
	//function has to do the task

	//first, find the matching document/s(single or array of documents)
		//manipulating database returns a promise
		//.then() handles the promise
			//resolve
			//reject
	return User.find().then( (result) => {
		// console.log(result); //array of documents

		return result;

	//return the  matching document

	})
}


module.exports.register = (reqBody) => {
	// Object is captured
	console.log(reqBody);
	

	//find the matching document using model method
	return User.findOne({email: reqBody.email}).then( (result) => {
		console.log(result)

		//if result != null, return false/`user exist`
		//if result == null, save the user
		if(result != null){
			return `User already exists`
		} else {

			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				password: reqBody.password,
				task: reqBody.task
			})

			return newUser.save().then( (result) => {
					return `User saved!`
			})
		}
	});
}

module.exports.getUser = (reqBody) => {
	//console.log(reqBody);

	return User.find({_id: reqBody._id})


}

module.exports.updateTask = (reqBody) => {
	//console.log(reqBody)

	User.findByIdAndUpdate({_id: reqBody._id}, (err, result) => {
	//	console.log(result);
	});
}